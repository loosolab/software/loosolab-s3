# S3_Python_loosolab_Package

This Package contains functions to work with the most recent Kubernetesobjects.

**Installation**
------------
Clone the git Repository, create a python conda environment and run the setup.py.

```bash
$ git clone git@gitlab.gwdg.de:loosolab/software/loosolab-s3.git
$ cd loosolab-s3/
$ python setup.py install
```

**Description of Functions**
------------
*check if bucket exists*

```bash
$ S3 --secret <secret>  --key <user name> --bucket_exists --bucketname <bucketname>
```
