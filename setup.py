from setuptools import setup

setup(name='loosolab_s3',
      version='0.0.1',
      description='',
      author='Marina Kiweler',
      author_email='marina.kiweler@mpi-bn.mpg.de',
      license='MIT',
      packages=['loosolab_s3'],
      entry_points = {
        'console_scripts': ['S3 = loosolab_s3.cmd:run_s3_functions']
      },
      install_requires=[
        'boto3', # ==1.21.25',
      'botocore'],
      classifiers = [
        'Programming Language :: Python :: 3'
      ],
      zip_safe=False,
      include_package_data=True)
