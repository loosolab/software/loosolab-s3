#!/usr/bin/env python

"""
Class containing S3 functions based on boto3

Initialize object of class with Parameter:
		credentials : dictionary containing "signature", "key", "secret" and "endpoint"

"""

import boto3
import botocore
import hashlib
import logging
import os
import random
import re
import sys
import time

from botocore.client import Config

class Loosolab_s3:
# loger level deactivate and exceptions
#--------------Init -------------------------------------------------------------------------------------#
	def __init__(self, credentials, multipart_upload=True, logger=True):
		
		if logger:
			level = 20
		else:
			level = 50
	
		
		self.logger = logging.basicConfig( format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		self.logger = logging.getLogger('Loosolab_s3_logger')
		self.logger.setLevel(level)
		#self.logger = logging.getLogger().setLevel(level)
		

		# having a global session (needed for all boto action)
		self.create_s3_session(credentials)
		if not multipart_upload:
			self.create_s3_transfer(credentials)
		self.multipart_upload = multipart_upload


	def __exception_log__(self, information, exception_e=""):
		''' raise exception and logg as ERROR
		exception: e
		information: string
		'''
		self.logger.error(information + " " + str(exception_e))
		raise Exception(information + " " + str(exception_e))

#--------------------------------------------------------------------------------------------------------#
#------------- Resource / Client & Transfer	-------------------------------------------------------------#
	def create_s3_session(self, credentials, resource=True):
		""" Creating s3 session with boto3 - needed for any use of the s3 storage
		Parameter:
		----------
		credentials : dictionary
			contains : "signature", "key", "secret" and "endpoint"
		resource : boolean
			session as resource or as client
		"""
		try:
			# Default value for signature
			if not 'signature' in credentials:
				credentials['signature'] = 's3'

			# Store configuration once? 
			if resource:
				session = boto3.resource(
					's3',
					config=Config(signature_version=credentials["signature"]),
					aws_access_key_id=credentials["key"],
					aws_secret_access_key=credentials["secret"],
					endpoint_url=credentials["endpoint"]
				)
			else:	# client:
				session = boto3.client(
					's3',
					config=Config(signature_version=credentials["signature"]),
					aws_access_key_id=credentials["key"],
					aws_secret_access_key=credentials["secret"],
					endpoint_url=credentials["endpoint"]
				)
			client = boto3.client(
					's3',
					config=Config(signature_version=credentials["signature"]),
					aws_access_key_id=credentials["key"],
					aws_secret_access_key=credentials["secret"],
					endpoint_url=credentials["endpoint"]
				)
			self.session = session
			self.client = client
			self.check_s3_credentials()
		except Exception as e:
			self.__exception_log__("Problem with input!",e) 	

	#--------------------------------------------------------------------------------------------------------#
	def create_s3_transfer(self, credentials):
		""" Creating s3 transfer with boto3 - needed for upload on the s3 storage non_multipart
		Parameter:
		----------
		credentials : dictionary
		"""
		try:
			session = self.create_s3_session(credentials, resource=False)
			myconfig = boto3.s3.transfer.TransferConfig(
				multipart_threshold=9999999999999999, # workaround for 'disable' auto multipart upload
				max_concurrency=10,
				num_download_attempts=10,
			)
			transfer=boto3.s3.transfer.S3Transfer(session, myconfig)
			self.logger.info('S3 transfer created!')
			self.transfer = transfer
		except Exception as e:
			self.__exception_log__('S3 transfer could not be created! ',e)	
	
	#--------------------------------------------------------------------------------------------------------#
	#--------- Getter ---------------------------------------------------------------------------------------#
	def get_session(self):
		return self.session
	
	#--------------------------------------------------------------------------------------------------------#
	def get_transfer(self):
		if self.multipart_upload:
			print("ERROR: Transfer is only created when multipart_upload = False !")
			return
		else:	
			return self.transfer

#--------------------------------------------------------------------------------------------------------#
#------------- Checks -----------------------------------------------------------------------------------#
	def check_s3_credentials(self):
		""" Checking if credentials are correct by calling 'buckets.all'
		"""
		try:
			response = self.get_bucket_names()
			self.logger.info('S3 session created!')
		except Exception as e:
			self.__exception_log__('Used wrong credentials could not conect to S3! ', e) 

	#--------------------------------------------------------------------------------------------------------#
	def check_s3_bucket_ex(self, bucket_name):
		""" check if bucket exists
		Parameter:
		----------
		bucket_name : string
			bucket name as string
		Returns:
		-------
		Boolean
			does bucket exist?
		"""

		try:
			buckets = [bucket.name for bucket in self.session.buckets.all()]
			
			if bucket_name in buckets:
				return True
			else: 
				return False
		except botocore.exceptions.ClientError as e:
			if not e.response['Error']['Code'] == "404":
				self.__exception_log__("Something went wrong checking for " + bucket_name, e)
			return False


	#--------------------------------------------------------------------------------------------------------#
	def check_s3_object_ex(self, bucket_name, file_name):
		""" check if file on s3 storage exists in named bucket 
		Parameter:
		----------
		bucket_name : string
			bucket name as string
		file_name : string
			Name of file on s3 storage
		Returns:
		-------
		Boolean
			does file exist?
		"""
		if self.check_s3_bucket_ex(bucket_name):
			try:
				self.session.Object(bucket_name, file_name).load()
			except botocore.exceptions.ClientError as e:
				if not e.response['Error']['Code'] == "404":
					self.__exception_log__("Something went wrong checking for " + file_name + " in " + bucket_name, e)
				return False
			# no except -> the object does exist.
			return True
		else:
			return False
	
	#--------------------------------------------------------------------------------------------------------#
	def compare_s3_etag(self, bucket_name, file_name, local_file_name):
		""" compare local and s3 files 
		Parameter:
		----------
		bucket_name : string
			bucket name as string
		file_name : string
			Name of file on s3 storage
		local_file_name : string / path
			Path to local file
		Returns:
		-------
		modBool : Boolean
			has file changed?
		"""
		try:
			s3_object = self.session.Object(bucket_name, file_name)
			try:
				s3_e_tag = s3_object.e_tag[1:-1]		# etag without quotes
			except:	# object does not exist
				return False

			if '-' in s3_e_tag:
				local_tag = self.etag_checksum(local_file_name)
			else:	
				local_tag = hashlib.md5(open(local_file_name).read().encode('utf-8')).hexdigest()	
			
			self.logger.info('local e-tag of ' + local_file_name +' is : ' + local_tag)
			self.logger.info('s3 e-tag of ' + file_name +' is : ' + s3_e_tag)

			modBool = local_tag == s3_e_tag
			if modBool:
				self.logger.info("Files are not changed! " + str(file_name))
			return modBool

		except Exception as e:
			return False
	
	#--------------------------------------------------------------------------------------------------------#
	def confirm_bucket_name(self, bucket_name):	# todo laenge automatisch anpassen?
		""" checks if entered bucket name is valid and alters '_' or '.'.
		Parameter:
		------
		bucket_name : string

		Returns:
		--------
		bucket_name : string
		"""
		bucket_name = bucket_name.lower()	#only lowercase is allowed
		if ('_' or '.') in bucket_name: #bucket must not contain '_' or '.'
			bucket_name = bucket_name.replace('_','-').replace('.','')
			self.logger.warning('There are not supported characters in your bucket name (\".\" or \"_\") they are replaced. New name is: ' + bucket_name)
		name_len = len(bucket_name)
		if name_len > 63 or name_len < 3:#bucket name length must be between 63 and 3
			self.__exception_log__('The bucket name must consist of 3 to 63 characters, the entered name has a length of '+ str(name_len), 'Bucket name '+ bucket_name +  ' invalid')

		return bucket_name

#--------------------------------------------------------------------------------------------------------#
#------------- Non-S3 Utils -----------------------------------------------------------------------------#
	def etag_checksum(self, file_name, chunk_size=8 * 1024 * 1024):	# https://zihao.me/post/calculating-etag-for-aws-s3-objects/
		""" calculates the etag for Multi-uploaded files
		Parameter:
		----------
		file_name : string
			path to local file
		chunk_size : int
			size of upload chunks (8MB as default)
		Returns:
		--------
			Recalculated e-Tag of local file
		"""
		# zu langsam!
		md5s = []
		with open(file_name, 'rb') as f:
			for data in iter(lambda: f.read(chunk_size), b''):
				md5s.append(hashlib.md5(data).digest())
		m = hashlib.md5(b"".join(md5s))
		return '{}-{}'.format(m.hexdigest(), len(md5s))
	
	#--------------------------------------------------------------------------------------------------------#
	def _create_random(charamount):
		""" create random string
		Parameter:
		----------
		charamount : int
		lenght of random string
		Returns:
		--------
		string	
		"""
		empty_string = ''
		random_str = empty_string.join(random.choice(string.ascii_lowercase + string.digits) for char in range(charamount))
		return(random_str)	

#--------------------------------------------------------------------------------------------------------#
#------------- Bucket Management ------------------------------------------------------------------------#
	def get_bucket_names(self, pattern=""):
		""" Get a list of all buckets
		Parameter:
		----------
		pattern : string
		
		Returns:
		--------
		bucket_name_list : list
				contains names of all buckets
		"""
		bucket_name_list = []
		try:
			for bucket in self.session.buckets.all():
				bucket_name_list.append(bucket.name)
			if not pattern:	# retun ALL bucket names
				return bucket_name_list

			else:	 # with pattern matching
				match_list = [bucket_name for bucket_name in bucket_name_list if re.search(pattern, bucket_name)]
				self.logger.info('Found the following buckets: ' + str(match_list))
				
				if len(match_list) == 0:
					self.__exception_log__('No matching buckets for {0} were found.'.format(pattern), 'No list returned')
				return match_list

		except Exception as e: 
			self.__exception_log__('Buckets could not be listed. ',e)

	#--------------------------------------------------------------------------------------------------------#
	def create_s3_bucket(self, bucket_name, name_addition=True):
		"""Creating an s3 bucket.
		Parameter:
		----------
		bucket_name : string
		name_addition : Boolean
			if bucket name should be altered when occupied
		Returns:
		--------
		bucket_name : string
		"""
		bucket_name = self.confirm_bucket_name(bucket_name)
		
		name_occupied = True
		i = 3	# counter to exit the loop if 3 name aditions do not work
		while name_occupied:
			try:
				bucket = self.session.create_bucket(Bucket=bucket_name)
				name_occupied = False
				self.logger.info('Bucket ' + bucket_name +' was created!')
			# If bucket already exist but is not owned by you, add random string:
			except self.session.meta.client.exceptions.BucketAlreadyExists as e:
				if (name_addition and i > 0 and len(bucket) < 53):	# 3 versuche, max length
					addition = self._create_random(10)
					bucket_name = bucket_name + "_" + addition
					i = i-1
				else:
					self.__exception_log__(' Bucket name is already occupied by another user!', e)
					return
			# If bucket is already owned by you go on as if it was created
			except self.session.meta.client.exceptions.BucketAlreadyOwnedByYou as e:
				bucket = self.session.Bucket(bucket_name)
				self.logger.info('Bucket ' + bucket_name +' is already owned by you!')
				name_occupied = False
			except Exception as e:
				self.__exception_log__(' Bucket could not be created!',e)
				return
		return bucket_name

	#--------------------------------------------------------------------------------------------------------#
	def emptie_s3_buckets(self, bucket_names, delete_bucket = False):
		""" Delete and emptie bucket
		Parameter:
		----------
		bucket_names : string or list of bucket names
		delete_bucket : Bool
			if True bucket will be deleted
		"""
		if isinstance(bucket_names, str):
			bucket_list = [bucket_names]
		else:
			bucket_list = bucket_names
			
		for bucket_name in bucket_list:
			try:
				bucket = self.session.Bucket(bucket_name)
			except Exception as e: 
				self.logger.error('Bucket does not exist.' + str(e))
				continue
			try:
				bucket.objects.all().delete()
				if not delete_bucket:
					self.logger.info("Bucket " + bucket_name + " emptied.")
					continue
			except Exception as e:	# todo Was ist wenn keine Objects da sind? 
				self.logger.error('Bucket Objects could not be deleted.' + str(e))
			try:
				bucket.delete()
				self.logger.info("Bucket " + bucket_name + " deleted.")
			except Exception as e:
				self.__exception_log__('Bucket could not be deleted.' ,e)

#--------------------------------------------------------------------------------------------------------#
#-------------- File Management -------------------------------------------------------------------------#
	def get_object_names(self, bucket_name, pattern=""):
		""" Get a list of all objects in bucket
		Parameter:
		----------
		bucket_name : string
		pattern : string
		
		Returns:
		--------
		object_name_list : list
			contains names of all objects in bucket
		"""
		object_name_list = []
		if not self.check_s3_bucket_ex(bucket_name):
			self.logger.error('Bucket does not exist!')
			return []
		try:
			bucket = self.session.Bucket(bucket_name)
		except Exception as e:
			self.logger.error('Problem calling bucket. ' + str(e))
			return
		try:
			for obj in bucket.objects.all():
				object_name_list.append(obj.key)
			if not pattern:
				return object_name_list
			else:
				match_list = [obj for obj in object_name_list if re.search(pattern, obj)]
				self.logger.info('Found the following files: ' + str(match_list))
				if len(match_list) == 0:
					self.logger.error('No matching files for {0} were found in bucket {1}.'.format(pattern,bucket_name))
				return match_list
		except Exception as e:
			self.logger.error('Objects in Bucket ' + bucket_name + 'could not be listed! ' + str(e))

	#--------------------------------------------------------------------------------------------------------#
	def upload_s3_objects(self, bucket_name, file_list, compare=True):
		"""Creating an s3 bucket.
		Parameter:
		----------
		bucket_name : string
		file_list : list
			list of local filepaths
		compare : boolean
			should local and s3 file be compared?	

		Returns:
		--------
			Boolean
		"""

		modBool = False
		if not self.check_s3_bucket_ex(bucket_name):
			self.__exception_log__("Bucket for upload does not exist!")
			return
		count = 1 
		for local_file_name in file_list:
			try:
				file_name = os.path.basename(local_file_name)
				if compare:
					modBool = self.compare_s3_etag(bucket_name, file_name, local_file_name)
				if not modBool:
					if self.multipart_upload:
						self.session.Bucket(bucket_name).upload_file(local_file_name, file_name)
					else:
						self.transfer.upload_file(local_file_name, bucket_name, file_name)
						#self.session.Bucket(bucket_name).upload_file(local_file_name, file_name, Config=self.transfer)
					self.logger.info(str(count) + '/' + str(len(file_list)) + " File " +  str(local_file_name) + ' gets uploaded! ' )
					count += 1 
			except Exception as e:
				self.__exception_log__("S3: Uploading files failed!" ,e)
				return
		return True

	#--------------------------------------------------------------------------------------------------------#
	def download_s3_objects(self, bucket_name, file_list, destination=os.getcwd(), compare=True, download_bar=False):
		""" Download files from bucket
		Parameter:
		----------
		bucket_name : string
		file_list : list
			Files to download
		destination : string
		timeout : float
			how long to watch for file in minutes
		"""

		
		bucket = self.session.Bucket(bucket_name)
		modBool = False
		# check if exists
		try:
			for local_file in file_list:
				file_name = os.path.basename(local_file)
				file_path = os.path.join(destination, local_file)
				if compare:
					modBool = self.compare_s3_etag(bucket_name, file_name, file_path)
				if self.check_s3_object_ex(bucket_name, file_name) and not modBool:
					if not os.path.exists(os.path.dirname(file_path)):
						os.makedirs(os.path.dirname(file_path))
						self.logger.info('Created directory: ' + os.path.dirname(file_path))
					if self.multipart_upload and not download_bar:
						bucket.download_file(file_name, file_path)
					elif download_bar:
						
						self.download(file_path,bucket_name,  file_name)
					else:
						bucket.download_file(file_name, file_path, Config=self.transfer)
		except Exception as e:
			self.__exception_log__("Could not download file " + file_name , e)

#--------------------------------------------------------------------------------------------------------#

	def download(self, local_file_name, s3_bucket, s3_object_key):
		
		meta_data = self.client.head_object(Bucket=s3_bucket, Key=s3_object_key)
		total_length = float(meta_data.get('ContentLength', 0))
		downloaded = 0
		part = total_length / 100
		count = total_length / 100
		def progress(chunk):
			nonlocal downloaded
			nonlocal part
			nonlocal count
			downloaded += chunk
			
			if downloaded > count:
				print(str(int((100 / total_length ) * count)) + ' % Downloaded!', end="\r")
				#print("\r[%s%s]" % (str(int((100 / total_length ) * count)) + ' % Downloaded!') , end="\r")
				count += part
			
			#print("\r[%s%s]" % ('=' * done, ' ' * (50-done)) , end="\r")
			#sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )
			#sys.stdout.flush()

		self.logger.info(f'Downloading {s3_object_key}')
		with open(local_file_name, 'wb') as f:
			self.client.download_fileobj(s3_bucket, s3_object_key, f, Callback=progress)  

		self.logger.info(f'Downloading finished {s3_object_key}')