#!/usr/bin/env python
""" CMD function for loosolab s3 """

import argparse
import sys
import os

#own functions
from loosolab_s3.Utils import Utils
from loosolab_s3.s3_functions import Loosolab_s3


#--------------------------------------------------------------------------------------------------------#
#- MAIN -------------------------------------------------------------------------------------------------#
def run_s3_functions():
    utils = Utils()
    args = argparsefunc()

    #start s3 session
    credentials = {
        "endpoint":args.endpoint,
        "key":args.key,
        "secret": args.secret,
        "signature":args.signature
        }
    if args.no_log:
        s3 = Loosolab_s3(credentials, logger=False)
    else:
        s3 = Loosolab_s3(credentials)

    if args.output:
        output = args.output
    else:
        output = os.getcwd()     
    if args.read_form_dict:
        bucket_dict = args.read_form_dict  
        bucketname = bucket_dict.split(':')[0]
        files = [bucket_dict.split(':')[1]]
    if args.bucketname:
        bucketname = args.bucketname
    if args.files:
        files = args.files
    if bucketname != s3.confirm_bucket_name(bucketname):
        s3.logger.warning('Warning Bucket name: '+ bucketname +  ' invalid. Change to: ' + s3.confirm_bucket_name(bucketname))
        bucketname = s3.confirm_bucket_name(bucketname)
    #------------------------------------------------actions--------------------------------------------#

    #check if bucket exists
    #do NOT change this output in any way as this is being parsed by other tools (ckuenne: deploy_igv.sh)
    if args.bucket_exists:
        utils.check_argparser(args, ['secret', "key", 'bucketname'])
        if s3.check_s3_bucket_ex(bucketname):
            print('Found bucket ' +  bucketname + ': yes')
        else:
            print('Found bucket ' +  bucketname + ': no')
    
    if args.upload:
        if args.read_form_dict:
            utils.check_argparser(args, ['secret', "key", "read_form_dict"])
        else:    
            utils.check_argparser(args, ['secret', "key", "files", "bucketname"])
        s3.upload_s3_objects(bucketname, files)
    if args.download:
        if args.read_form_dict:
            utils.check_argparser(args, ['secret', "key", "read_form_dict"])
        else:    
            utils.check_argparser(args, ['secret', "key", "files", "bucketname"])
        
        s3.download_s3_objects( bucketname, files, destination=output, download_bar=True)

    if args.create_bucket:
        utils.check_argparser(args, ['secret', "key", 'bucketname'])
        s3.create_s3_bucket(bucketname, name_addition=True) 

    if args.delete_bucket:
        utils.check_argparser(args, ['secret', "key", 'bucketname'])
        s3.emptie_s3_buckets(bucketname, delete_bucket = True)
#--------------------------------------------------------------------------------------------------------#
# parse command line arguments:
def argparsefunc():
    parser = argparse.ArgumentParser(
        description="", add_help=False)
    parser.add_argument('--key', help="user name")	
    parser.add_argument("--secret", help="s3 secret")	
    parser.add_argument("--signature", help="s3  signature",default='s3v4')
    parser.add_argument("--endpoint", help="URL S3",default='https://s3.mpi-bn.mpg.de')

    #actions
    parser.add_argument("--upload", action='store_true', help="upload files to bucket")
    parser.add_argument("--download", action='store_true', help="download files to bucket")
    parser.add_argument("--bucket_exists", action='store_true', help="check if bucket exists")
    parser.add_argument("--create_bucket", action='store_true', help="create bucket")
    parser.add_argument("--delete_bucket", action='store_true', help="create bucket")

    #values
    #values
    parser.add_argument("--read_form_dict", help="Read from dict <bucket_name:file_name>")
    parser.add_argument("--files", nargs='+', help="bucket upload files")
    parser.add_argument("--bucketname")
    parser.add_argument("--no_log", action='store_true', help='disable logging')
    parser.add_argument("--output", help='Download folder')

    parser.add_argument('-h', '--help', action='store_true', dest='show_help')
    args = parser.parse_args()

    if args.show_help:
        parser.print_help(sys.stderr)
        sys.exit(1)
    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit(1)    
    return args

if __name__ == "__main__":
   run_s3_functions()

