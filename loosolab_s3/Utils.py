#!/usr/bin/env python3
import os
import sys
import time
import logging

class Utils:
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
    def is_valid_file(self, parser, arg):
        if not os.path.exists(arg):
            parser.error("The file %s does not exist!" % arg)
        else:
            return open(arg, 'r')  # return an open file handle

#--------------------------------------------------------------------------------------------------------#

    def check_if_file_exists(self, files):
        if len(files) == 0:
            logging.error( "Their are no Files given!")
            sys.exit(1)
        for i in files:
            if os.path.exists(i):
                pass
            else:
                logging.error('File ' + i + ' didn\'t exist!')
                sys.exit(1)

#--------------------------------------------------------------------------------------------------------#

    def query_yes_no(self, question, default="yes"):
        valid = {"yes": True, "y": True, "ye": True,
                 "no": False, "n": False}
        if default is None:
            prompt = " [y/n] "
        elif default == "yes":
            prompt = " [Y/n] "
        elif default == "no":
            prompt = " [y/N] "
        else:
            raise ValueError("invalid default answer: '%s'" % default)
        while True:
            sys.stdout.write(question + prompt)
            choice = input().lower()
            if default is not None and choice == '':
                return valid[default]
            elif choice in valid:
                return valid[choice]
            else:
                sys.stdout.write("Please respond with 'yes' or 'no' "
                                 "(or 'y' or 'n').\n")

#--------------------------------------------------------------------------------------------------------#

    def waiting_points(self, message):
        time.sleep(0.25)
        print(message + ' {0}'.format('.   '), end='\r')
        time.sleep(0.25)
        print(message + ' {0}'.format('..  '), end='\r')
        time.sleep(0.25)
        print(message + ' {0}'.format('... '), end='\r')
        time.sleep(0.25)
        print(message + ' {0}'.format('....'), end='\r')

#--------------------------------------------------------------------------------------------------------#

    def check_argparser(self,args, required):
        for arg in required:
            if getattr(args, arg) == None:
                sys.exit("ERROR: Missing argument --{0}".format(arg))