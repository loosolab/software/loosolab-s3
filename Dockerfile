FROM python

COPY  . .



RUN apt-get update && apt-get install -y \
    python3-pip

RUN python setup.py install


RUN groupadd -g 999 appuser && \
    useradd -r -u 999 -g appuser appuser

RUN chown appuser /home/

USER appuser